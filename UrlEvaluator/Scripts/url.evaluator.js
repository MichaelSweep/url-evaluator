﻿feather.replace()
var ctx = document.getElementById("myChart");
myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [],
        datasets: [{
            data: [],
            lineTension: 0,
            backgroundColor: 'transparent',
            borderColor: '#007bff',
            borderWidth: 4,
            pointBackgroundColor: '#007bff'
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        legend: {
            display: false,
        }
    }
});

$("#evaluateUrlForm").on('submit', function (e) {
    e.preventDefault();
    $("#results_block").show("slow");
    getSitemapLinks($(this).find('#inputUrl').val());
});

function getSitemapLinks(link) {
    $.ajax({
        url: '/Home/GetUrl/',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: '{ Url:"' + link + '" }',
        success: function (links, textStatus) {
            if (links.fail) {
                alert(links.message);
            } else {
                links.forEach(function (id) {
                    getUrlResponseTime(id);
                });
            }
        }
    });
}

function getUrlResponseTime(id) {
    $.ajax({
        url: '/Home/GetResponseTime/',
        type: 'POST',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: '{ Id:"' + id + '" }',
        success: function (link, textStatus) {
            if (link.responseTime != null) {
                addUrlResponseTimeToPlot(link.Url, link.responseTime.replace(/,/g, '.'));
            }
            showUrlResponseTimeTable(link.Url, link.responseTime);
        }
    });
}

function addUrlResponseTimeToPlot(url, responseTime)
{
    myChart.data.labels.push(url);
    myChart.data.datasets[0].data.push(responseTime);
    myChart.update();
}

evaluatedUrls = Array();
function showUrlResponseTimeTable(url, responseTime) {
    if (responseTime == null) {
        evaluatedUrls.push({ "Url": url, "ResponseTime": null });
    }
    else {
        evaluatedUrls.push({ "Url": url, "ResponseTime": parseFloat(responseTime.replace(/,/g, '.')) });
    }
    drawResultsTable(evaluatedUrls);
}

function drawResultsTable() {
    var view = '';
    evaluatedUrlsSorted = evaluatedUrls.sort(sortJsonResponseTime);
    $.each( evaluatedUrlsSorted, function (index, element) {
        if(element.ResponseTime != null){
            view += '<tr><td><a href="' + element.Url + '">' + element.Url + '</a></td><td>' + element.ResponseTime + '</td></tr>';
        }
        else {
            view += '<tr class="error"><td><a href="' + element.Url + '">' + element.Url + '</a></td><td></td></tr>';
        }
    });

    $("#results").html(view);
}


function sortJsonResponseTime(a, b) {
    return  b.ResponseTime - a.ResponseTime;
}
