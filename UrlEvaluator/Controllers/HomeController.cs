﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UrlEvaluator.Models;
using HtmlAgilityPack;
using System.Net;
using System.Data;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace UrlEvaluator.Controllers
{
    public class HomeController : Controller
    {
        private UrlsDBEntities db = new UrlsDBEntities();

        //
        // GET: /

        public ActionResult Index()
        {
            return View();
        }


        //
        // POST: /

        [HttpPost]
        public ActionResult GetUrl(string Url)
        {
            if (!Url.StartsWith("http"))
            {
                Url = "http://" + Url;
            }
            EnteredUrls enteredurl = new EnteredUrls();
            enteredurl.Url = Url;
            db.EnteredUrls.Add(enteredurl);
            db.SaveChanges();

            HtmlWeb hw = new HtmlWeb();
            Uri myUri = new Uri(Url);

            try
            {
                HtmlDocument doc = hw.Load(Url);

                List<int> evaluatedUrls = new List<int>();
                foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
                {
                    HtmlAttribute att = link.Attributes["href"];

                    string uriHost = myUri.Host;

                    if (att.Value.StartsWith("//"))
                    {
                        att.Value = "http:" + att.Value;
                    }
                    // if link url is relative
                    else if (att.Value.StartsWith("/") || att.Value.StartsWith("#"))
                    {
                        if (!myUri.Host.StartsWith("http:") || !myUri.Host.StartsWith("https:"))
                        {
                            uriHost = "http://" + myUri.Host;
                        }
                        att.Value = uriHost + att.Value;
                    }
                    // to avoid url evaluating duplicating and javascript triggers
                    else if (att.Value == "#" || att.Value.StartsWith("#") || att.Value.StartsWith("javascript:"))
                    {
                        continue;
                    }

                    EvaluatedUrls evurl = new EvaluatedUrls();
                    evurl.Url = att.Value;
                    evurl.EnteredUrlId = enteredurl.Id;
                    evurl.ResponseSpeed = null;
                    db.EvaluatedUrls.Add(evurl);
                    db.SaveChanges();

                    evaluatedUrls.Add(evurl.Id);
                }

                return Json(evaluatedUrls);
            }
            catch
            {
                return Json(new { fail = true, message = "Entered URL is not correct!" });
            }
        }


        //
        // POST: /

        [HttpPost]
        public JsonResult GetResponseTime(int Id)
        {

            EvaluatedUrls evUrl = new EvaluatedUrls();
            evUrl = db.EvaluatedUrls.Find(Id);

            //request.Timeout = 5000; // set timeot to 5 seconds
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(evUrl.Url);

            System.Diagnostics.Stopwatch timer = new Stopwatch();
            timer.Start();
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                response.Close();
            }
            catch { }
            timer.Stop();

            TimeSpan ts = timer.Elapsed;
            string elapsedTime = String.Format("{0:0},{1:000}",
                ts.Seconds,
                ts.Milliseconds).ToString();

            evUrl.ResponseSpeed = Double.Parse(elapsedTime);
            db.Entry(evUrl).State = EntityState.Modified;
            db.SaveChanges();
            return Json(new Dictionary<string, string>() { { "Url", evUrl.Url }, { "responseTime", elapsedTime } });
        }
    }
}