﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UrlEvaluator.Models;
using HtmlAgilityPack;
using System.Net;
using System.Diagnostics;

namespace UrlEvaluator.Controllers
{ 
    public class HistoryController : Controller
    {
        private UrlsDBEntities db = new UrlsDBEntities();

        //
        // GET: /History/

        public ViewResult Index()
        {
            return View(db.EnteredUrls.OrderByDescending(x => x.Id).ToList());
        }

        //
        // GET: /History/Details/5

        public ViewResult Details(int id)
        {
            EnteredUrls enteredurl = db.EnteredUrls.Find(id);
            List<EvaluatedUrls> EvaluatedUrlsList = enteredurl.EvaluatedUrls.OrderByDescending(x => x.ResponseSpeed).ToList();
            ViewBag.enteredUrl = enteredurl;

            return View(EvaluatedUrlsList);
        }
    }
}