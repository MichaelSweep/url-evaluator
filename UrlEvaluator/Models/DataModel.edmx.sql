
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 02/01/2018 18:50:38
-- Generated from EDMX file: C:\!Projects\UrlEvaluator\UrlEvaluator\Models\DataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [UrlsDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_EnteredUrlId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EvaluatedUrls] DROP CONSTRAINT [FK_EnteredUrlId];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[EnteredUrls]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EnteredUrls];
GO
IF OBJECT_ID(N'[dbo].[EvaluatedUrls]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EvaluatedUrls];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'EnteredUrls'
CREATE TABLE [dbo].[EnteredUrls] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Url] nvarchar(max)  NULL
);
GO

-- Creating table 'EvaluatedUrls'
CREATE TABLE [dbo].[EvaluatedUrls] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Url] nvarchar(max)  NULL,
    [EnteredUrlId] int  NULL,
    [ResponseSpeed] float  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'EnteredUrls'
ALTER TABLE [dbo].[EnteredUrls]
ADD CONSTRAINT [PK_EnteredUrls]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EvaluatedUrls'
ALTER TABLE [dbo].[EvaluatedUrls]
ADD CONSTRAINT [PK_EvaluatedUrls]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [EnteredUrlId] in table 'EvaluatedUrls'
ALTER TABLE [dbo].[EvaluatedUrls]
ADD CONSTRAINT [FK_EnteredUrlId]
    FOREIGN KEY ([EnteredUrlId])
    REFERENCES [dbo].[EnteredUrls]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_EnteredUrlId'
CREATE INDEX [IX_FK_EnteredUrlId]
ON [dbo].[EvaluatedUrls]
    ([EnteredUrlId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------