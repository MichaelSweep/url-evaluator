//------------------------------------------------------------------------------
// <auto-generated>
//    Этот код был создан из шаблона.
//
//    Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//    Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UrlEvaluator.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EvaluatedUrls
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public Nullable<int> EnteredUrlId { get; set; }
        public Nullable<double> ResponseSpeed { get; set; }
    
        public virtual EnteredUrls EnteredUrls { get; set; }
    }
}
